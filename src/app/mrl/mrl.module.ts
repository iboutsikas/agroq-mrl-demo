import { NgModule } from '@angular/core';
import { MrlDisplayListComponent } from '@mrl/components/mrl-display-list/mrl-display-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { ReactiveFormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/common/app-common.module';
import { MrlDashboardComponent } from './components/mrl-dashboard/mrl-dashboard.component';
import { MrlFilterSelectorComponent } from './components/mrl-filter-selector/mrl-filter-selector.component';
import { mrlReducer } from '@mrl/store';

const routes: Routes = [
  { path: 'mrls', component: MrlDashboardComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    StoreModule.forFeature('residue_levels', mrlReducer),
    NgbModule,
    AppCommonModule,
    NgxMyDatePickerModule
  ],
  declarations: [
    MrlDashboardComponent,
    MrlFilterSelectorComponent,
    MrlDisplayListComponent
  ]
})
export class MrlModule { }
