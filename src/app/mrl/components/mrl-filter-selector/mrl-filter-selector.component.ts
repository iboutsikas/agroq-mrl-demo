import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TypeAheadEventItem } from '@app/common/components/focus-type-ahead/TypeAheadEventItem';
import { INgxMyDpOptions, NgxMyDatePickerDirective } from 'ngx-mydatepicker';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { map } from 'rxjs/operators';

import { ActiveIngredient, Commodity, Country } from '@models';
import { pipe } from 'rxjs/Rx';





@Component({
  selector: 'app-mrl-filter-selector',
  templateUrl: './mrl-filter-selector.component.html',
  styleUrls: ['./mrl-filter-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MrlFilterSelectorComponent implements OnInit, OnChanges {

  filterForm: FormGroup;
  datepicker_options: INgxMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    closeSelectorOnDocumentClick: true
  };

  @Input() countries: Country[];
  @Input() ingredients: ActiveIngredient[];
  @Input() commodities: Commodity[];

  selectedCountry: Country;
  selectedIngredient: ActiveIngredient;
  selectedCommodity: Commodity;

  constructor(private fb: FormBuilder) {

  }

  ngOnInit() {
    this.filterForm = this.fb.group({
      filter_country: '',
      filter_ingredient: '',
      filter_product: '',
      filter_from: '',
      filter_to: ''
    });
  }

  filterCountries = (term$: Observable<string>, countries: Country[]) => {
    return pipe(
      map((text: string) => {
        const lowerCaseTerm = text.trim().toLowerCase();

        const list = (lowerCaseTerm === '') ? countries :
          countries
          .filter(c => c.name.toLowerCase().indexOf(lowerCaseTerm) > -1 ||
            c.shorthand.toLowerCase().indexOf(lowerCaseTerm) > -1
          ).slice(0, 10);

        return list;
      })
    );
  }

  filterIngredients = (term$: Observable<string>, ingredients: ActiveIngredient[]) => {
    return pipe(
      map((text: string) => {
        const lowerCaseTerm = text.trim().toLowerCase();
        const list = (lowerCaseTerm === '') ? ingredients :
          ingredients.filter(i => i.name.toLowerCase().indexOf(lowerCaseTerm) > -1)
            .slice(0, 10);
        return list;
      })
    );
  }

  filterCommodities = (term$: Observable<string>, commodities: Commodity[]) => {
    return pipe(
      map((text: string) => {
        const lowerCaseTerm = text.trim().toLowerCase();
        const list = (lowerCaseTerm === '') ? commodities :
          commodities.filter(i => i.name.toLowerCase().indexOf(lowerCaseTerm) > -1)
            .slice(0, 10);
        return list;
      })
    );
  }

  onCountrySelected(event: TypeAheadEventItem) {
    this.selectedCountry = event.entity;
  }

  onIngredientSelected(event: TypeAheadEventItem) {
    this.selectedIngredient = event.entity;
  }

  onCommoditySelected(event: TypeAheadEventItem) {
    this.selectedCommodity = event.entity;
  }

  countriesFormatter = (result: Country): string => `${result.name} (${result.shorthand})`;

  ingredientsFormatter = (result: ActiveIngredient): string => result.name;

  commoditiesFormatter = (result: Commodity): string => result.name;

  ngOnChanges(): void {
  }

  filter(): void {
    console.log(this.filterForm.getRawValue());
  }

  onDatePickerFocus(datepicker: NgxMyDatePickerDirective) {
    console.log('focus');
    datepicker.openCalendar();
  }

  onDatePickerBlur(datepicker: NgxMyDatePickerDirective) {
    console.log('blur');
    datepicker.closeCalendar();
  }

}
