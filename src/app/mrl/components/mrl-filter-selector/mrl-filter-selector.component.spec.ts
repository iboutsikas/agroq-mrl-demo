import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MrlFilterSelectorComponent } from './mrl-filter-selector.component';

describe('MrlFilterSelectorComponent', () => {
  let component: MrlFilterSelectorComponent;
  let fixture: ComponentFixture<MrlFilterSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MrlFilterSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MrlFilterSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
