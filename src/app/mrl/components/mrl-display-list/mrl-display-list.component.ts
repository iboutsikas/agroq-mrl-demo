import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ComponentBase } from '@app/common/componentBase';
import { MRL } from '@models';
import { Store } from '@ngrx/store';
import { MRLSelectors } from '@mrl/store';

@Component({
  selector: 'app-mrl-display-list',
  templateUrl: './mrl-display-list.component.html',
  styleUrls: ['./mrl-display-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MrlDisplayListComponent extends ComponentBase implements OnInit, OnDestroy {

  mrls: MRL[];

  constructor(private store: Store<any>) {
    super();
    this.disposeOnDestroy(
     this.store.select(MRLSelectors.getMRLs)
       .subscribe(data => this.mrls = data)
    );
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {

  }
}
