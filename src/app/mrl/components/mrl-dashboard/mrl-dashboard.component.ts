import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { IngredientSelectors } from '@ingredients/store';
import { ActiveIngredient, Commodity, Country } from '@models';
import { Observable } from 'rxjs/Observable';
import { CommoditiesSelectors } from '@commodities/store';
import { CountriesSelectors } from '@countries/store';

@Component({
  selector: 'app-mrl-dashboard',
  templateUrl: './mrl-dashboard.component.html',
  styleUrls: ['./mrl-dashboard.component.scss']
})
export class MrlDashboardComponent implements OnInit {

  countries$: Observable<Country[]>;

  ingredients$: Observable<ActiveIngredient[]>;

  commodities$: Observable<Commodity[]>;

  constructor(private store: Store<any>) {
    this.ingredients$ = this.store.select(IngredientSelectors.selectIngredients);
    this.commodities$ = this.store.select(CommoditiesSelectors.getCommodities);
    this.countries$ = this.store.select(CountriesSelectors.getCountries);
  }

  ngOnInit() {
  }

}
