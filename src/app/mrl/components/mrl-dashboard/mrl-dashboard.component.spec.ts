import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MrlDashboardComponent } from './mrl-dashboard.component';

describe('MrlDashboardComponent', () => {
  let component: MrlDashboardComponent;
  let fixture: ComponentFixture<MrlDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MrlDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MrlDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
