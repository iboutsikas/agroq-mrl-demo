export interface MrlFetchRequest {
  country_id?: number;
  ingredient_id?: number;
  product_id?: number;
  date_from?: Date;
  date_to?: Date;
  page: number;
}
