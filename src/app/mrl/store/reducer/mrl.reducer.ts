import { ActiveIngredient, Commodity, Country, MRL } from '@models';
import * as MrlActions from '@mrl/store/actions/mrl.actions';

const countries: Country[] = [
  { id: 1, name: 'Greece', shorthand: 'gr' },
  { id: 2, name: 'Russia', shorthand: 'ru' },
  { id: 3, name: 'Japan', shorthand: 'jp' }
];

const ingredients: ActiveIngredient[] = [
  { id: 1, name: 'Abamectin' },
  { id: 2, name: 'Acephate' },
  { id: 3, name: 'Acequinocyl' },
  { id: 4, name: 'Acetamiprid (R)' }
];

const commodities: Commodity[] = [
  { id: 1, name: 'Peach' },
  { id: 2, name: 'Pear' },
  { id: 3, name: 'Kiwi' },
  { id: 4, name: 'Persimmon' },
  { id: 5, name: 'Pomefruit' }
];

const mrls: MRL[] = [];
let mrlIndex = 1;

for (const c of countries) {
  for (const ing of ingredients) {
    for (const com of commodities) {
      const mrl: MRL = {
        id: mrlIndex,
        residue: Math.random(),
        from_date: new Date('1/1/2016'),
        to_date: new Date('1/1/2017')
      };

      mrl.active_ingredient = ing;
      mrl.country = c;
      mrl.commodity = com;

      mrls.push(mrl);
      mrlIndex++;
    }
  }
}
export interface MRLState {
  data: MRL[];
  loaded: boolean;
  loading: boolean;
}

export const initialState: MRLState = {
  data: mrls,
  loaded: false,
  loading: false
};

export function mrlReducer(state: MRLState = initialState, action: MrlActions.ActionType): MRLState {

  switch (action.type) {
    case MrlActions.LOAD_MRLS: {
      return Object.assign({}, state, { loading: true });
    }

    case MrlActions.LOAD_MRLS_SUCCESS: {
      return Object.assign({}, state, { loading: false, loaded: true });
    }

    case MrlActions.LOAD_MRLS_FAIL: {
      return Object.assign({}, state, { loading: false, loaded: false });
    }
    default: return state;
  }
}
