import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MRLState } from './reducer/mrl.reducer';
import { MRL } from '@models';

const getMrlState = createFeatureSelector<MRLState>('residue_levels');

export const getMRLs = createSelector(
  getMrlState,
  (state: MRLState): MRL[] => state.data
);

export const getMRLsLoading = createSelector(
  getMrlState,
  (state: MRLState): boolean => state.loading
);

export const getMRLsLoaded = createSelector(
  getMrlState,
  (state: MRLState): boolean => state.loaded
);
