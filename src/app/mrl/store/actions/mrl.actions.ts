import { Action } from '@ngrx/store';
import { MrlFetchRequest } from '@mrl/requests';
import { MRL } from '@models';

export const LOAD_MRLS = '[MRLs] Load Paged';
export const LOAD_MRLS_SUCCESS = '[MRLs] Load Paged Success';
export const LOAD_MRLS_FAIL = '[MRLs] Load Paged Fail';

export class LoadMRLs implements Action {
  readonly type = LOAD_MRLS;
  constructor(public payload: MrlFetchRequest) {

  }
}

export class LoadMRLsSuccess implements Action {
  readonly type = LOAD_MRLS_SUCCESS;
  constructor(public payload: MRL[]) {}
}

export class LoadMrlsFail implements Action {
  readonly type = LOAD_MRLS_FAIL;
  constructor(public payload: any) {}
}

export type ActionType = LoadMRLs | LoadMRLsSuccess | LoadMrlsFail;
