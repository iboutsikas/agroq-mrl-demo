import { Commodity } from '@models';
import { Action } from '@ngrx/store';

export const LOAD_COMMODITIES = '[Commodities] Load';
export const LOAD_COMMODITIES_SUCCESS = '[Commodities] Load Success';
export const LOAD_COMMODITIES_FAIL = '[Commodities] Load Fail';

export class LoadCommodities implements Action {
  readonly type = LOAD_COMMODITIES;
  constructor() {}
}

export class LoadCommoditiesSuccess implements Action {
  readonly type = LOAD_COMMODITIES_SUCCESS;
  constructor(public payload: Commodity[]) {}
}

export class LoadCommoditiesFail implements Action {
  readonly type = LOAD_COMMODITIES_FAIL;
  constructor(public payload: any) {}
}

export type ActionType = LoadCommodities | LoadCommoditiesSuccess | LoadCommoditiesFail;
