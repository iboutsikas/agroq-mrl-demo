import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CommodityState } from './reducer/commodities.reducer';

const getCommoditiesState = createFeatureSelector<CommodityState>('commodities');

export const getCommodities = createSelector(
  getCommoditiesState,
  (state: CommodityState) => state.data
);
