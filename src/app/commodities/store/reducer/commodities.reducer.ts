import { Commodity } from '@models';
import * as CommoditiesActions from '../actions/commodities.actions';

const commodities: Commodity[] = [
  { id: 1, name: 'Peach' },
  { id: 2, name: 'Pear' },
  { id: 3, name: 'Kiwi' },
  { id: 4, name: 'Persimmon' },
  { id: 5, name: 'Pomefruit' }
];

export interface CommodityState {
  data: Commodity[];
}

const initialState: CommodityState = {
  data: commodities
};

export function commodityReducer(
  state: CommodityState = initialState,
  action: CommoditiesActions.ActionType): CommodityState {

  switch (action.type) {
    case CommoditiesActions.LOAD_COMMODITIES_SUCCESS: {
      const a = action as CommoditiesActions.LoadCommoditiesSuccess;
      return { data: a.payload };
    }

    default: return state;
  }

}
