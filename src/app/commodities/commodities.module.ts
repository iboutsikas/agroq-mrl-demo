import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { commodityReducer } from '@commodities/store';
import { StoreModule } from '@ngrx/store';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('commodities', commodityReducer)
  ],
  declarations: []
})
export class CommoditiesModule { }
