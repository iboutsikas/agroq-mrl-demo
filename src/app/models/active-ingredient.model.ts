export interface ActiveIngredient {
  id?: number;
  name: string;
  eu_id?: number;
  category?: string;
  category_id?: number;
}
