import { Country } from '@models';
import { ActiveIngredient } from '@models';
import { Commodity } from '@models';

export interface MRL {
  id: number;
  residue: number;
  country?: Country;
  active_ingredient?: ActiveIngredient;
  commodity?: Commodity;
  from_date: Date;
  to_date: Date;
}
