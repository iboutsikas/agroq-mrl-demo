export interface IngredientCategory {
  id: number;
  name: string;
  shorthand: string;
}
