export interface Country {
  id: number;
  name: string;
  shorthand: string;
}
