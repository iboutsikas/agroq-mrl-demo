export const LOAD_CATEGORIES = '[Categories]Load All';
export const LOAD_CATEGORIES_SUCCESS = '[Categories]Load All Success';
export const LOAD_CATEGORIES_FAIL = '[Categories]Load All Fail';
