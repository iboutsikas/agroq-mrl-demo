import { Component } from '@angular/core';
import { ComponentBase } from '@app/common/componentBase';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { UiSelectors } from '@app/core/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends ComponentBase {

  sidebarOpen$: Observable<boolean>;
  // sidebarOpen: boolean;

  constructor(private store: Store<any>) {
    super();

    this.sidebarOpen$ = this.store
      .select(UiSelectors.getSidebarOpen)
      .do(_ => console.log(_));

    // this.disposeOnDestroy(this.sidebarOpen$.subscribe(v => this.sidebarOpen = v));
  }
}
