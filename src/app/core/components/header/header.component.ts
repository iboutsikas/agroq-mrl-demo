import { Component, OnInit } from '@angular/core';
import { ComponentBase } from '@app/common/componentBase';
import { Store } from '@ngrx/store';
import { UiActions } from '@app/core/store';

const breakpoints = {
  'sm': 576,
  'md': 768,
  'lg': 922,
  'xl': 1200
};

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends ComponentBase implements OnInit {

  public isSidebarOpen = true;

  constructor(private store: Store<any>) {
    super();
  }

  ngOnInit() {
    const width = window.innerWidth;
    this.isSidebarOpen = width > breakpoints[ 'md' ];

    if (this.isSidebarOpen) {
      this.store.dispatch(new UiActions.OpenSidebar());
    } else {
      this.store.dispatch(new UiActions.CloseSidebar());
    }
  }

  onTogglePressed(): void {
    this.isSidebarOpen = !this.isSidebarOpen;

    if (this.isSidebarOpen) {
      this.store.dispatch(new UiActions.OpenSidebar());
    } else {
      this.store.dispatch(new UiActions.CloseSidebar());
    }
  }

}
