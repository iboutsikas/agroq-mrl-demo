import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SidenavComponent } from '@app/core/components/sidenav/sidenav.component';
import { coreReducers } from '@app/core/store/reducers';
import { StoreModule } from '@ngrx/store';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    StoreModule.forFeature('core', coreReducers)
  ],
  declarations: [
    HomeComponent,
    SidenavComponent,
    HeaderComponent
  ],
  exports: [
    HomeComponent,
    SidenavComponent,
    HeaderComponent
  ]
})
export class CoreModule { }
