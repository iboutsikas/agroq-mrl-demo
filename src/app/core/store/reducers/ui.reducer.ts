import * as UiActions from '../actions/ui.actions';
export interface UIState {
  sidebar_open: boolean;
}

const initialState: UIState = {
  sidebar_open: false
};

export function uiReducer(state: UIState = initialState, action: UiActions.ActionType): UIState {
  switch (action.type) {

    case UiActions.OPEN_SIDEBAR: {
     return Object.assign({}, state, { sidebar_open: true});
    }

    case UiActions.CLOSE_SIDEBAR: {
      return Object.assign({}, state, { sidebar_open: false});
    }

    default: return state;
  }
}
