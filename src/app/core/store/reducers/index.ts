import { uiReducer, UIState } from '@app/core/store/reducers/ui.reducer';
import { ActionReducerMap } from '@ngrx/store';

export interface CoreState {
  ui: UIState;
}

export const coreReducers: ActionReducerMap<CoreState> = {
  ui: uiReducer
};
