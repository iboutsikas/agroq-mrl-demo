import { CoreState } from '@app/core/store/reducers';
import { UIState } from '@app/core/store/reducers/ui.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';

const getCoreState = createFeatureSelector('core');

export const getUiState = createSelector(
  getCoreState,
  (state: CoreState): UIState => state.ui
);

export const getSidebarOpen = createSelector(
  getUiState,
  (state: UIState): boolean => state.sidebar_open
);
