import { Action } from '@ngrx/store';

export const OPEN_SIDEBAR = '[UI] Open Sidebar';
export const CLOSE_SIDEBAR = '[UI] Close Sidebar';

export class OpenSidebar implements Action {
  readonly type = OPEN_SIDEBAR;
  constructor() {}
}

export class CloseSidebar implements Action {
  readonly type = CLOSE_SIDEBAR;
  constructor() {}
}

export type ActionType = OpenSidebar | CloseSidebar;
