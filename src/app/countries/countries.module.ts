import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { countriesReducer } from '@countries/store';
import { StoreModule } from '@ngrx/store';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('countries', countriesReducer)
  ],
  declarations: []
})
export class CountriesModule { }
