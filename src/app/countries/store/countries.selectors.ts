import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CountryState } from './reducer/countries.reducer';

const getCountriesState = createFeatureSelector<CountryState>('countries');

export const getCountries = createSelector(
  getCountriesState,
  (state: CountryState) => state.data
);
