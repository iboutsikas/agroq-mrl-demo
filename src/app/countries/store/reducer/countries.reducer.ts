import { Commodity, Country } from '@models';
import * as CountriesActions from '../actions/countries.actions';

const countries: Country[] = [
  { id: 1, name: 'Greece', shorthand: 'gr' },
  { id: 2, name: 'Russia', shorthand: 'ru' },
  { id: 3, name: 'Japan', shorthand: 'jp' }
];

export interface CountryState {
  data: Country[];
}

const initialState: CountryState = {
  data: countries
};

export function countriesReducer(
  state: CountryState = initialState,
  action: CountriesActions.ActionType): CountryState {

  switch (action.type) {
    case CountriesActions.LOAD_COUNTRIES_SUCCESS: {
      const a = action as CountriesActions.LoadCountriesSuccess;
      return { data: a.payload };
    }

    default: return state;
  }

}
