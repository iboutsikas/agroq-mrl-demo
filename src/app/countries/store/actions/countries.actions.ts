import { Country } from '@models';
import { Action } from '@ngrx/store';

export const LOAD_COUNTRIES = '[Countries] Load';
export const LOAD_COUNTRIES_SUCCESS = '[Countries] Load Success';
export const LOAD_COUNTRIES_FAIL = '[Countries] Load Fail';

export class LoadCountries implements Action {
  readonly type = LOAD_COUNTRIES;
  constructor() {}
}

export class LoadCountriesSuccess implements Action {
  readonly type = LOAD_COUNTRIES_SUCCESS;
  constructor(public payload: Country[]) {}
}

export class LoadCountriesFail implements Action {
  readonly type = LOAD_COUNTRIES_FAIL;
  constructor(public payload: any) {}
}

export type ActionType = LoadCountries | LoadCountriesSuccess | LoadCountriesFail;
