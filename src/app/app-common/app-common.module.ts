import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FocusTypeAheadComponent } from './components/focus-type-ahead/focus-type-ahead.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    NgbModule
  ],
  declarations: [
    FocusTypeAheadComponent

  ],
  exports: [
    FocusTypeAheadComponent

  ]
})
export class AppCommonModule { }
