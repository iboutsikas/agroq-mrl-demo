import { Component, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs/Subject';
import { debounceTime, distinctUntilChanged, merge } from 'rxjs/operators';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TypeAheadEventItem } from './TypeAheadEventItem';

const VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef( () => FocusTypeAheadComponent),
  multi: true
};

@Component({
  selector: 'app-focus-type-ahead',
  templateUrl: './focus-type-ahead.component.html',
  styleUrls: ['./focus-type-ahead.component.scss'],
  providers: [VALUE_ACCESSOR]
})
export class FocusTypeAheadComponent implements OnInit, ControlValueAccessor {

  @Input() searchFunc;
  @Input() resultFormatter;
  @Input() elementId;
  @Input() cssClasses;
  @Input() data;
  @Input() searchDelay = 200;

  @Output() itemSelected: EventEmitter<TypeAheadEventItem> = new EventEmitter<TypeAheadEventItem>();

  @ViewChild('instance') instance: NgbTypeahead;
  @ViewChild('input') input;

  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  constructor() { }

  ngOnInit() {
    if (!this.resultFormatter) {
      console.error('You did not provide a result formatter for the typeahead.');
    }
  }

  filterResults = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(this.searchDelay),
      distinctUntilChanged(),
      merge(this.focus$),
      merge(this.click$),
      this.searchFunc(text$, this.data)
    )

  onItemSelected(event) {
    this.itemSelected.emit({ id: event.item.id, entity: event.item });
  }

  public clear(): void {
    this.instance.writeValue(null)
    this.itemSelected.emit({ id: null, entity: null });
  }

  writeValue(obj: any): void {
    this.instance.writeValue(obj);
  }

  registerOnChange(fn: any): void {
    this.instance.registerOnChange(fn);
  }

  registerOnTouched(fn: any): void {
    this.instance.registerOnTouched(fn);
  }

}
