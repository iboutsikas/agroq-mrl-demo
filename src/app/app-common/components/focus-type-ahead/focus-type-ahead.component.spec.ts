import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FocusTypeAheadComponent } from './focus-type-ahead.component';

describe('FocusTypeAheadComponent', () => {
  let component: FocusTypeAheadComponent;
  let fixture: ComponentFixture<FocusTypeAheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FocusTypeAheadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FocusTypeAheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
