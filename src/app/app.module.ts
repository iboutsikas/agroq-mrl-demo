import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from '@app/core/core.module';
import { CountriesModule } from '@countries/countries.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { environment } from '../environments/environment';
import { CommoditiesModule } from '@commodities/commodities.module';
import { IngredientsModule } from '@ingredients/ingredients.module';
import { MrlModule } from '@mrl/mrl.module';

const dev_imports = [];

if (!environment.production) {
  dev_imports.push(
    StoreDevtoolsModule.instrument({
      maxAge: 25
    })
  );
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({}, {}),
    EffectsModule.forRoot([]),
    NgbModule.forRoot(),
    ...dev_imports,
    NgxMyDatePickerModule.forRoot(),
    CoreModule,
    IngredientsModule,
    MrlModule,
    CommoditiesModule,
    CountriesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
