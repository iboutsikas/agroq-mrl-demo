import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IngredientsEffects } from '@ingredients/store/ingredients.effects';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ingredientReducer } from './store/reducer/ingredients.reducer';
import { IngredientsListComponent } from './components/ingredients-list/ingredients-list.component';
import { NewIngredientComponent } from './components/new-ingredient/new-ingredient.component';

const routes: Routes = [
  {
    path: 'active-ingredients', children: [
      { path: 'new', component: NewIngredientComponent },
      { path: '', component: IngredientsListComponent, pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('ingredients', ingredientReducer),
    EffectsModule.forFeature([IngredientsEffects])
  ],
  declarations: [IngredientsListComponent, NewIngredientComponent]
})
export class IngredientsModule { }
