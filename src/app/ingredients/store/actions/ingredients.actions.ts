import { Action } from '@ngrx/store';
import { ActiveIngredient } from '@models';

export const LOAD_INGREDIENTS = '[INGREDIENTS] Load';
export const LOAD_INGREDIENTS_SUCCESS = '[INGREDIENTS] Load Success';
export const LOAD_INGREDIENTS_FAIL = '[INGREDIENTS] Load Fail';
export const LOAD_INGREDIENT = '[INGREDIENT] Load One';
export const LOAD_INGREDIENT_SUCCESS = '[INGREDIENT] Load One Success';
export const LOAD_INGREDIENT_FAIL = '[INGREDIENT] Load One Fail';
export const CREATE_INGREDIENT = '[Ingredient] Create';
export const CREATE_INGREDIENT_SUCCESS = '[Ingredient] Create Success';
export const CREATE_INGREDIENT_FAIL = '[Ingredient] Create Fail';

export class LoadIngredients implements Action {
  readonly type = LOAD_INGREDIENTS;
  constructor() {}
}

export class LoadIngredientsSuccess implements Action {
  readonly type = LOAD_INGREDIENTS_SUCCESS;
  constructor(public payload: ActiveIngredient[]) {}
}

export class LoadIngredientsFail implements Action {
  readonly type = LOAD_INGREDIENTS_FAIL;
  constructor(public payload: any) {}
}

export class LoadIngredient implements Action {
  readonly type = LOAD_INGREDIENT;
  constructor(public payload: number) {}
}

export class LoadIngredientSuccess implements Action {
  readonly type = LOAD_INGREDIENT_SUCCESS;
  constructor(public payload: ActiveIngredient) {}
}

export class LoadIngredientFail implements Action {
  readonly type = LOAD_INGREDIENT_FAIL;
  constructor(public payload: any) {}
}

export class CreateIngredient implements Action {
  readonly type = CREATE_INGREDIENT;
  constructor(public payload: ActiveIngredient) {}
}

export class CreateIngredientSuccess implements Action {
  readonly type = CREATE_INGREDIENT_SUCCESS;
  constructor(public payload: ActiveIngredient) {}
}

export class CreateIngredientFail implements Action {
  readonly type = CREATE_INGREDIENT_FAIL;
  constructor(public payload: any) {}
}

export type ActionType = LoadIngredients | LoadIngredientsSuccess | LoadIngredientsFail
  | LoadIngredient | LoadIngredientSuccess | LoadIngredientFail
  | CreateIngredient | CreateIngredientSuccess | CreateIngredientFail;
