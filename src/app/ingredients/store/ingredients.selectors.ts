import { ingredientsAdapter, IngredientState } from './reducer/ingredients.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';


const {
  selectIds,
  selectAll,
  selectEntities,
} = ingredientsAdapter.getSelectors();

const selectState = createFeatureSelector<IngredientState>('ingredients');

export const selectIngredients = createSelector(selectState, selectAll);
export const selectIngredientsDictionary = createSelector(selectState, selectEntities);

