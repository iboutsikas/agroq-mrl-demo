import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActiveIngredient } from '@models';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { IngredientActions } from '@ingredients/store';
import { map, mergeMap, catchError,  } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { BASE_API_URL } from '../../app.constants';

@Injectable()
export class IngredientsEffects {
  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
  constructor(private actions$: Actions, private http: HttpClient) {}

  @Effect()
  loadIngredients$ = this.actions$
    .pipe(
      ofType(IngredientActions.LOAD_INGREDIENTS),
      mergeMap(action =>
        this.http.get(`${BASE_API_URL}/active-ingredients`)
          .pipe(
            map((data: ActiveIngredient[]) => new IngredientActions.LoadIngredientsSuccess(data)),
            catchError((err) => of(new IngredientActions.LoadIngredientsFail(err)))
          )
      )
    );

  @Effect()
  createIngredient$ = this.actions$
    .pipe(
      ofType(IngredientActions.CREATE_INGREDIENT),
      map((action: IngredientActions.CreateIngredient) => {
        console.log('CreateIngredient action payload:', action.payload);
        return action.payload;
      }),
      mergeMap(json =>
        this.http.post(`${BASE_API_URL}/active-ingredients`, json)
          .pipe(
            map((data: ActiveIngredient) => new IngredientActions.CreateIngredientSuccess(data)),
            catchError((err) => of(new IngredientActions.CreateIngredientFail(err)))
          )
      )
    );
}
