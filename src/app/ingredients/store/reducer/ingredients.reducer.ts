import { ActiveIngredient } from '@models';
import * as IngredientActions from '@ingredients/store/actions/ingredients.actions';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

export interface IngredientState extends EntityState<ActiveIngredient> {

}

export const ingredientsAdapter:  EntityAdapter<ActiveIngredient> = createEntityAdapter();

export const initialState: IngredientState = ingredientsAdapter.getInitialState();

export function ingredientReducer(state: IngredientState = initialState, action: IngredientActions.ActionType): IngredientState {
  switch (action.type) {
    case IngredientActions.LOAD_INGREDIENTS_SUCCESS: {
      const a = action as IngredientActions.LoadIngredientsSuccess;
      return ingredientsAdapter.addAll(a.payload, state);
    }

    case IngredientActions.LOAD_INGREDIENT_SUCCESS: {
      const a = action as IngredientActions.LoadIngredientSuccess;
      return ingredientsAdapter.addOne(a.payload, state);
    }

    case IngredientActions.CREATE_INGREDIENT_SUCCESS: {
      const a = action as IngredientActions.CreateIngredientSuccess;
      return ingredientsAdapter.addOne(a.payload, state);
    }
    default: return state;
  }
}
