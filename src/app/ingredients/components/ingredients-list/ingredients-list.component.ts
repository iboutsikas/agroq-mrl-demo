import { Component, OnInit } from '@angular/core';
import { ActiveIngredient } from '@models';
import { Store } from '@ngrx/store';
import { IngredientSelectors, IngredientActions } from '@ingredients/store';
@Component({
  selector: 'app-ingredients-list',
  templateUrl: './ingredients-list.component.html',
  styleUrls: ['./ingredients-list.component.scss']
})
export class IngredientsListComponent implements OnInit {

  ingredients: ActiveIngredient[];

  constructor(private store: Store<any>) {
    this.store.select(IngredientSelectors.selectIngredients)
      .subscribe(ings => this.ingredients = ings);
  }

  ngOnInit() {
    this.store.dispatch(new IngredientActions.LoadIngredients());
  }

}
