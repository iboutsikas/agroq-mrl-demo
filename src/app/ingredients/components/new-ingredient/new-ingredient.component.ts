import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActiveIngredient } from '@models';
import { Store } from '@ngrx/store';
import { IngredientActions } from '@ingredients/store';
@Component({
  selector: 'app-new-ingredient',
  templateUrl: './new-ingredient.component.html',
  styleUrls: ['./new-ingredient.component.scss']
})
export class NewIngredientComponent implements OnInit {

  private newForm: FormGroup;

  constructor(private fb: FormBuilder, private store: Store<any>) { }

  ngOnInit() {
    this.newForm = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      category: [0, Validators.compose([Validators.required])],
      eu_id: [0]
    });
  }

  public onSubmit(): void {

    const ing: ActiveIngredient = {
      name: this.newForm.controls['name'].value,
      category_id: this.newForm.controls['category'].value,
      eu_id: this.newForm.controls['eu_id'].value
    };

    console.log(ing);

    this.store.dispatch(new IngredientActions.CreateIngredient(ing));
  }

}
