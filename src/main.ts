import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import fontawesome from '@fortawesome/fontawesome';
import { faCalendarAlt } from '@fortawesome/fontawesome-free-regular';
import { faWindowClose } from '@fortawesome/fontawesome-free-solid';

fontawesome.library.add(faCalendarAlt, faWindowClose);

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
